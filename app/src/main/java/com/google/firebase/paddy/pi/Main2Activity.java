package com.google.firebase.paddy.pi;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ImageView image = (ImageView) findViewById(R.id.imageView2);

        //image.setImageResource(R.drawable.i);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Intent homeIntent =new Intent(Main2Activity.this,MainActivity.class);
                startActivity(homeIntent);
                finish();
            }
        }, 3000);

    }
    public void button(View v)
    {
        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
    }

}
