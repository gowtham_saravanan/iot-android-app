# IoT Project Overview 

This App was developed to monitor the real-time sensor data from the paddy godown
to predict the quality of the stored paddy. PaddyPi uses the Firebase Cloud Messaging (FCM) and
Firebase Real-time Database for real-time data acquisition from the field.

The Embedded side consists of a Raspberry Pi (Microprocessor) acting as an MQTT Broker and
Wemos D1 (Microcontroller) acting as Nodes. Nodes collect data from the Digital Humidity and
Temperature Sensor (DHT-11) and Gas sensor to monitor the quality of the paddy. Raspberry PI
acting as the central controller collects the data from the nodes, predicts the quality of the
paddy from the sensor values and updates the quality of the nodes in the Firebase Real-time
Database. And if the quality decreases, the user is notified using notifications on the PaddyPi
using Firebase Cloud Messaging.

# Functionalities of the APP

- Authentication
- Subscribe to sensor data from **Firebase Real Time Database** (Raspberry pi publishes the data collecting from the other nodes)
- Receive critical notifications from raspberry pi using **Firebase Messaging Service**
- Share the sensor details via Mail 

# Screenshots

![Welcome Screen](images/paddypi.png)
